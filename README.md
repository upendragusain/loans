# README #

### Repository description ###

* Repo Purpose - **Submitting a new loan applications and then being able to review them**

* Make sure solution is set to multiple starup projects (Loans.API >> Loans.WebMvc >> LoansReview.WebMvc)

Projects and thier purpose

* **Loans.Infrastructure** - Database project (**Migratons** will be run at startup, hence might slow down a bit)

* **Loans.API** - web api project exposing various methods on loan resources

* **Loans.Engine** - .Net Standard library for loan APR etc calculations (notice also the test project added for this)

* **Loans.WebMvc** - web portal to create new loan application (makes http calls to Loans.API)

* **LoansReview.WebMvc** - **internal** web portal for reviewing submitted loan applications (makes http calls to Loans.API)



### Misc. ###
* APR calculations is done in a simple manner (assuming no extra fees that are added to the Loan)
* Main focus has been on scalability, extensiblity and lose coupling of interacting components.