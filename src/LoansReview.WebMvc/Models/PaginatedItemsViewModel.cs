﻿using LoansReview.WebMvc.Infrastructure;
using System.Collections.Generic;

namespace LoansReview.API.Application.Models
{
    public class PaginatedItemsViewModel
    {
        public int PageIndex { get; private set; }

        public int PageSize { get; private set; }

        public long Count { get; private set; }

        public IEnumerable<LoanApplicationVM> Data { get; private set; }

        public PaginatedItemsViewModel(
            int pageIndex, int pageSize, long count, 
            IEnumerable<LoanApplicationVM> data)
        {
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
            this.Count = count;
            this.Data = data;
        }
    }
}
