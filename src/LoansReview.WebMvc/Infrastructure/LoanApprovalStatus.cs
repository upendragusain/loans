﻿namespace LoansReview.WebMvc.Infrastructure
{
    public enum LoanApprovalStatus
    {
        InProgress,
        Approved,
        Declined
    }
}
