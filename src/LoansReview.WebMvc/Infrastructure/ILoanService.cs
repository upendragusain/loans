﻿using LoansReview.API.Application.Models;
using System.Threading.Tasks;

namespace LoansReview.WebMvc.Infrastructure
{
    public interface ILoanService
    {
        Task<PaginatedItemsViewModel> GetPage(
            int pageSize = 5,
            int pageIndex = 0);

        Task<LoanApplicationVM> Detail(
            string loanApplicationId);

        Task<string> UpdateLoanStatus(
            LoanApplicationVM vm);
    }
}