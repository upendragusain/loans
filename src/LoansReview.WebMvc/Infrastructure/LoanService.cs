﻿using LoansReview.API.Application.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace LoansReview.WebMvc.Infrastructure
{
    public class LoanService : ILoanService
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public LoanService(IHttpClientFactory httpClientFactory)
        {
            this._httpClientFactory = httpClientFactory;
        }

        public async Task<PaginatedItemsViewModel> GetPage(
            int pageSize = 5,
            int pageIndex = 0)
        {
            if (pageSize <= 0 || pageSize > 10)
                pageSize = 5;

            if (pageIndex < 0)
                pageIndex = 0;

            var client = _httpClientFactory.CreateClient("loanService");
            var response = await client.GetAsync(
                $"pages?pageSize={pageSize}&pageIndex={pageIndex}");

            response.EnsureSuccessStatusCode();

            var stringData = await response.Content.ReadAsStringAsync();
            var page = JsonConvert.DeserializeObject<PaginatedItemsViewModel>(stringData);
            return page;
        }

        public async Task<LoanApplicationVM> Detail(
            string loanApplicationId)
        {
            if (string.IsNullOrWhiteSpace(loanApplicationId))
                return null;

            var client = _httpClientFactory.CreateClient("loanService");
            var response = await client.GetAsync(
                $"get?loanApplicationId={loanApplicationId}");

            response.EnsureSuccessStatusCode();

            var stringData = await response.Content.ReadAsStringAsync();
            var loan = JsonConvert.DeserializeObject<LoanApplicationVM>(stringData);
            return loan;
        }

        public async Task<string> UpdateLoanStatus(
            LoanApplicationVM vm)
        {
            var client = _httpClientFactory.CreateClient("loanService");

            var content = new StringContent(JsonConvert.SerializeObject(vm),
              System.Text.Encoding.UTF8, "application/json");

            var response = await client.PostAsync("updatestatus", content);

            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsStringAsync();
        }
    }
}
