﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace LoansReview.WebMvc.Infrastructure
{
    public class LoanApplicationVM
    {
        public string LoanApplicationId { get; set; }
        public decimal RequestAmount { get; set; }
        public decimal TotalAmountPayable { get; set; }
        public int TermLengthInMonths { get; set; }
        public decimal MonthlyPayment { get; set; }
        public decimal APR { get; set; }
        public UserVM User { get; set; }
        public LoanApprovalStatus ApprovalStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public List<SelectListItem> ApprovalStatuses =>
           GetStatuses();
        private List<SelectListItem> GetStatuses()
        {
            var result = new List<SelectListItem>();
            result.Add(new SelectListItem { Text = "In Progress", Value = "0" });
            result.Add(new SelectListItem { Text = "Approved", Value = "1" });
            result.Add(new SelectListItem { Text = "Declined", Value = "2" });

            return result;
        }
    }
}
