﻿using LoansReview.WebMvc.Infrastructure;
using LoansReview.WebMvc.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LoansReview.WebMvc.Controllers
{
    public class LoanReviewController : Controller
    {
        private readonly ILoanService _loanService;

        public LoanReviewController(
            ILoanService loanService)
        {
            _loanService = loanService;
        }

        public async Task<IActionResult> Index(
            [FromQuery] int pageSize = 5,
            [FromQuery] int pageIndex = 0)
        {
            try
            {
                var page = await _loanService.GetPage(pageSize: pageSize, pageIndex: pageIndex);
                return View(page);
            }
            catch (System.Exception ex)
            {
                return View("Error", new ErrorViewModel() { Message = ex.Message });
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string loanApplicationId)
        {
            try
            {
                var loan = await _loanService.Detail(loanApplicationId);
                return View(loan);
            }
            catch (System.Exception ex)
            {
                return View("Error", new ErrorViewModel() { Message = ex.Message });
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(LoanApplicationVM vm)
        {
            try
            {
                await _loanService.UpdateLoanStatus(vm);
                return RedirectToAction("Index");
            }
            catch (System.Exception ex)
            {
                return View("Error", new ErrorViewModel() { Message = ex.Message });
            }
        }
    }
}
