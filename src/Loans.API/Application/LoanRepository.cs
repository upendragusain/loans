﻿using Loans.API.Application.Models;
using Loans.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Loans.API.Application
{
    public class LoanRepository : ILoanRepository
    {
        private readonly LoanContext _loanContext;

        public LoanRepository(LoanContext loanContext)
        {
            _loanContext = loanContext;
        }

        public async Task<string> Save(Models.LoanApplication loanApplicationVM)
        {
            var dbApplication = MapToEntity(loanApplicationVM);
            await _loanContext.AddAsync(dbApplication);
            await _loanContext.SaveChangesAsync();

            return dbApplication.LoanApplicationId.ToString();
        }

        public async Task<int> UpdateLoanStatus(Models.LoanApplication loanApplicationVM)
        {
            var application = await _loanContext.LoanApplications.FindAsync(loanApplicationVM.LoanApplicationId);

            if (application == null)
                throw new Exception("Loan application not found");

            application.ApprovalStatus = (Infrastructure.LoanApprovalStatus)loanApplicationVM.ApprovalStatus;
            _loanContext.Attach(application);
            _loanContext.Entry(application).Property("ApprovalStatus").IsModified = true;
            return await _loanContext.SaveChangesAsync();
        }

        public async Task<Models.LoanApplication> Get(string loanReferenceId)
        {
            var application = await _loanContext.LoanApplications.FindAsync(loanReferenceId);

            if (application == null)
                throw new Exception("Loan application not found");

            var loanApplicationVM = MapToModel(application);

            return loanApplicationVM;
        }

        public async Task<PaginatedItemsViewModel> GetPage(
            int pageSize = 5,
            int pageIndex = 0)
        {
            var totalItems = await _loanContext.LoanApplications
               .LongCountAsync();

            if (totalItems < (pageSize * pageIndex))
                pageIndex = 0;

            var itemsOnPage = await _loanContext.LoanApplications
                .OrderBy(c => c.LoanApplicationId)
                .Skip(pageSize * pageIndex)
                .Take(pageSize)
                .Select(_ => MapToModel(_))
                .ToListAsync();

            var page = new PaginatedItemsViewModel(
                pageIndex, pageSize, totalItems, itemsOnPage);

            return page;
        }

        private static Infrastructure.LoanApplication MapToEntity(Models.LoanApplication vm)
        {
            return new Infrastructure.LoanApplication()
            {
                LoanApplicationId = Guid.NewGuid().ToString(),
                ApprovalStatus = Infrastructure.LoanApprovalStatus.InProgress,
                APR = vm.APR,
                RequestAmount = vm.RequestAmount,
                TotalAmountPayable = vm.TotalAmountPayable,
                CreatedDate = DateTime.UtcNow,
                MonthlyPayment = vm.MonthlyPayment,
                TermLengthInMonths = vm.TermLengthInMonths,
                User = new Infrastructure.User()
                {
                    Address = vm.User.Address,
                    AnnualIncomeInThousands = vm.User.AnnualIncomeInThousands,
                    CarRegistrationNumber = vm.User.CarRegistrationNumber,
                    Email = vm.User.Email,
                    FirstName = vm.User.FirstName,
                    IsHomeOwner = vm.User.IsHomeOwner,
                    LastName = vm.User.LastName,
                    Mobile = vm.User.Mobile
                }
            };
        }

        private static Models.LoanApplication MapToModel(Infrastructure.LoanApplication loanApplication)
        {
            return new Models.LoanApplication()
            {
                LoanApplicationId = loanApplication.LoanApplicationId.ToString(),
                ApprovalStatus = (Models.LoanApprovalStatus)loanApplication.ApprovalStatus,
                APR = loanApplication.APR,
                RequestAmount = loanApplication.RequestAmount,
                TotalAmountPayable = loanApplication.TotalAmountPayable,
                CreatedDate = DateTime.UtcNow,
                MonthlyPayment = loanApplication.MonthlyPayment,
                TermLengthInMonths = loanApplication.TermLengthInMonths,
                User = new Models.User()
                {
                    Address = loanApplication.User.Address,
                    AnnualIncomeInThousands = loanApplication.User.AnnualIncomeInThousands,
                    CarRegistrationNumber = loanApplication.User.CarRegistrationNumber,
                    Email = loanApplication.User.Email,
                    FirstName = loanApplication.User.FirstName,
                    IsHomeOwner = loanApplication.User.IsHomeOwner,
                    LastName = loanApplication.User.LastName,
                    Mobile = loanApplication.User.Mobile
                }
            };
        }
    }
}
