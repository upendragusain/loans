﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Loans.API.Application.Models
{
    public class LoanApplication
    {
        public string LoanApplicationId { get; set; }
        public decimal RequestAmount { get; set; }
        public decimal TotalAmountPayable { get; set; }
        public int TermLengthInMonths { get; set; }
        public decimal MonthlyPayment { get; set; }
        public decimal APR { get; set; }
        public User User { get; set; }
        public LoanApprovalStatus ApprovalStatus { get; set; }

        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
