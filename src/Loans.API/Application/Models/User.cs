﻿using System;

namespace Loans.API.Application.Models
{
    public class User
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return $"{FirstName} {LastName}".Trim(); } }
        public string Address { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public bool IsHomeOwner { get; set; }
        public string CarRegistrationNumber { get; set; }
        public decimal AnnualIncomeInThousands { get; set; }
    }
}