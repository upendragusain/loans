﻿using System.Collections.Generic;

namespace Loans.API.Application.Models
{
    public class PaginatedItemsViewModel
    {
        public int PageIndex { get; private set; }

        public int PageSize { get; private set; }

        public long Count { get; private set; }

        public IEnumerable<LoanApplication> Data { get; private set; }

        public PaginatedItemsViewModel(
            int pageIndex, int pageSize, long count, 
            IEnumerable<LoanApplication> data)
        {
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
            this.Count = count;
            this.Data = data;
        }
    }
}
