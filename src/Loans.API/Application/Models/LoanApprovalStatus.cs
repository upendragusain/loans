﻿namespace Loans.API.Application.Models
{
    public enum LoanApprovalStatus
    {
        InProgress,
        Approved,
        Declined
    }
}