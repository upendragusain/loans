﻿using Loans.API.Application.Models;
using System;
using System.Threading.Tasks;

namespace Loans.API.Application
{
    public interface ILoanRepository
    {
        Task<LoanApplication> Get(string loanReferenceId);
        Task<string> Save(LoanApplication loanApplicationVM);

        Task<PaginatedItemsViewModel> GetPage(
            int pageSize = 5,
            int pageIndex = 0);

        Task<int> UpdateLoanStatus(Models.LoanApplication loanApplicationVM);
    }
}