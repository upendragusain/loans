﻿using Loans.API.Application;
using Loans.API.Application.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Loans.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class LoanApplicationsController : ControllerBase
    {
        private readonly ILoanRepository _loanRepository;

        public LoanApplicationsController(ILoanRepository loanRepository)
        {
            _loanRepository = loanRepository;
        }

        [HttpPost]
        [Route("create")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> CreateAsync([FromBody] LoanApplication loanApplication)
        {
            try
            {
                var applicationReference = await _loanRepository.Save(loanApplication);
                return Ok(applicationReference);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        [HttpPost]
        [Route("updatestatus")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        public async Task<IActionResult> UpdateAsync([FromBody] LoanApplication loanApplication)
        {
            try
            {
                await _loanRepository.UpdateLoanStatus(loanApplication);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        [HttpGet]
        [Route("get")]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType(typeof(LoanApplication), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAsync(string loanApplicationId)
        {
            try
            {
                var application = await _loanRepository.Get(loanApplicationId);
                if (application == null)
                    return NotFound();

                return Ok(application);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }

        [HttpGet]
        [Route("pages")]
        [ProducesResponseType(typeof(PaginatedItemsViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<LoanApplication>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAllAsync(
            [FromQuery] int pageSize = 5, 
            [FromQuery] int pageIndex = 0)
        {
            try
            {
                if (pageSize <= 0 || pageSize > 10)
                    pageSize = 5;

                if (pageIndex < 0)
                    pageIndex = 0;

                var applications = await _loanRepository.GetPage(pageSize, pageIndex);

                return Ok(applications);
            }
            catch (Exception exception)
            {
                return BadRequest(exception.Message);
            }
        }
    }
}
