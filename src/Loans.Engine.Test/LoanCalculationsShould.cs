using System;
using System.Collections.Generic;
using Xunit;

namespace Loans.Engine.Test
{
    public class LoanCalculationsShould
    {
        private readonly LoanCalculations _loanCalculations;

        public LoanCalculationsShould()
        {
            _loanCalculations = new LoanCalculations();
        }

        [Theory]
        [MemberData(nameof(GetData))]
        public void ShouldCalculateAPR(
            decimal monthlyInterestRate,
            int loanTerm,
            decimal loanAmount,
            APRResult aprResult)
        {
            var result = _loanCalculations.GetAPR(
                monthlyInterestRate, loanTerm, loanAmount);

            Assert.Equal(aprResult.APR, result.APR);
            Assert.Equal(aprResult.CreditAmount, result.CreditAmount);
            Assert.Equal(aprResult.TotalAmountPayable, result.TotalAmountPayable);
            Assert.Equal(aprResult.RepaymentTerm, result.RepaymentTerm);
            Assert.Equal(aprResult.RepaymentAmount, result.RepaymentAmount);
        }

        public static IEnumerable<object[]> GetData()
        {
            yield return new object[] { 17.3m, 18, 550.0m,
                new APRResult(550.0m, 628.38m, 34.91m, 18, 17.3m)};

            yield return new object[] { 17.3m, 18, 1000.0m,
                new APRResult(1000.0m, 1142.51m, 63.47m, 18, 17.3m)};
        }
    }
}
