﻿using System;

namespace Loans.Infrastructure
{
    public class LoanApplication : BaseEntity
    {
        public string LoanApplicationId { get; set; }
        public decimal RequestAmount { get; set; }
        public decimal TotalAmountPayable { get; set; }
        public int TermLengthInMonths { get; set; }
        public decimal MonthlyPayment { get; set; }
        public decimal APR { get; set; }
        public User User { get; set; }
        public LoanApprovalStatus ApprovalStatus { get; set; }
    }
}