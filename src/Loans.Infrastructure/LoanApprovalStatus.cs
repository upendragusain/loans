﻿namespace Loans.Infrastructure
{
    public enum LoanApprovalStatus
    {
        InProgress,
        Approved,
        Declined
    }
}