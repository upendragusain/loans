﻿using Microsoft.EntityFrameworkCore;
using System;

namespace Loans.Infrastructure
{
    public class LoanContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<LoanApplication> LoanApplications { get; set; }
        public LoanContext(DbContextOptions<LoanContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=(localdb)\mssqllocaldb;Database=Loans;Integrated Security=True");
        }
    }
}
