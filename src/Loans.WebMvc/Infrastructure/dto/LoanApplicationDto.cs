﻿using System;

namespace Loans.WebMvc.Infrastructure.dto
{
    public class LoanApplicationDto
    {
        public Guid LoanApplicationId { get; set; }
        public decimal RequestAmount { get; set; }
        public decimal TotalAmountPayable { get; set; }
        public int TermLengthInMonths { get; set; }
        public decimal MonthlyPayment { get; set; }
        public decimal APR { get; set; }
        public UserDto User { get; set; }
        public int ApprovalStatus { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
