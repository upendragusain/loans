﻿using Loans.WebMvc.Infrastructure.dto;
using Loans.WebMvc.Models.Application;
using System.Threading.Tasks;

namespace Loans.WebMvc.Infrastructure
{
    public interface ILoanService
    {
        Task<string> Save(LoanApplicationDto vm);

        //Task<LoanApplicationVM> Get(string loanApplicationId);
    }
}