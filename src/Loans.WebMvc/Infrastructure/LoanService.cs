﻿using Loans.WebMvc.Infrastructure.dto;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Loans.WebMvc.Infrastructure
{
    public class LoanService : ILoanService
    {
        private readonly IHttpClientFactory _httpClientFactory;
        public LoanService(IHttpClientFactory httpClientFactory)
        {
            this._httpClientFactory = httpClientFactory;
        }

        public async Task<string> Save(LoanApplicationDto vm)
        {
            var client = _httpClientFactory.CreateClient("loanService");

            var content = new StringContent(JsonConvert.SerializeObject(vm), 
                System.Text.Encoding.UTF8, "application/json");
            var response = await client.PostAsync("create", content);

            response.EnsureSuccessStatusCode();

            var loanApplicationId = await response.Content.ReadAsStringAsync();
            return loanApplicationId;
        }
    }
}
