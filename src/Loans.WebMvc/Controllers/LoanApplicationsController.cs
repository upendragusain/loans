﻿using System;
using System.Threading.Tasks;
using Loans.Engine;
using Loans.WebMvc.Infrastructure;
using Loans.WebMvc.Infrastructure.dto;
using Loans.WebMvc.Models;
using Loans.WebMvc.Models.Application;
using Microsoft.AspNetCore.Mvc;

namespace Loans.WebMvc.Controllers
{
    public class LoanApplicationsController : Controller
    {
        private readonly ILoanCalculations loanCalculations;
        private readonly ILoanService loanService;

        public LoanApplicationsController(
            ILoanCalculations loanCalculations,
            ILoanService loanService)
        {
            this.loanCalculations = loanCalculations;
            this.loanService = loanService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View(new LoanApplicationVM());
        }

        [HttpPost]
        public async Task<ActionResult> Create(LoanApplicationVM vm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // get APR calculations
                    var aprResult = loanCalculations.GetAPR(17.3m, 18, vm.LoanRequestAmount);

                    var submissionVm = new LoanSubmissionVM
                    {
                        CreditAmount = aprResult.CreditAmount,
                        MonthlyPayment = aprResult.RepaymentAmount,
                        NumOfRepayments = aprResult.RepaymentTerm,
                        PayableAmount = aprResult.TotalAmountPayable,
                        APR = aprResult.APR
                    };

                    var loanDto = Map(vm, submissionVm);
                    var applicationRefrence = await loanService.Save(loanDto);
                    submissionVm.ApplicationRefrence = applicationRefrence;

                    return View("Submission", submissionVm);
                }
                else
                {
                    return View("Error", new ErrorViewModel() { Message = "Invalid data, please try again" });
                }
            }
            catch (Exception ex)
            {
                return View("Error", new ErrorViewModel() { Message = ex.Message });
            }
        }

        private static LoanApplicationDto Map(LoanApplicationVM loanApplicationVM, LoanSubmissionVM loanSubmissionVM)
        {
            return new LoanApplicationDto()
            {
                User = new UserDto()
                {
                    Address = loanApplicationVM.Address,
                    AnnualIncomeInThousands = loanApplicationVM.AnnualIncome,
                    CarRegistrationNumber = loanApplicationVM.CarRegistrationNumber,
                    Email = loanApplicationVM.EmailAddress,
                    FirstName = loanApplicationVM.FirstName,
                    LastName = loanApplicationVM.Lastname,
                    IsHomeOwner = loanApplicationVM.IsHomeOwner,
                    Mobile = loanApplicationVM.MobileNumber,
                },
                APR = loanSubmissionVM.APR,
                RequestAmount = loanApplicationVM.LoanRequestAmount,
                TotalAmountPayable = loanSubmissionVM.PayableAmount,
                MonthlyPayment = loanSubmissionVM.MonthlyPayment,
                TermLengthInMonths = loanSubmissionVM.NumOfRepayments
            };
        }
    }
}
