﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Loans.WebMvc.Models.Application
{
    public class LoanSubmissionVM
    {
        [DisplayName("Amount Of Credit")]
        public decimal CreditAmount { get; set; }

        [DisplayName("Total Amount Payable")]
        public decimal PayableAmount { get; set; }

        public int NumOfRepayments { get; set; }

        public decimal MonthlyPayment { get; set; }

        public string Repayments { get { return $"{NumOfRepayments} X £{Math.Round(MonthlyPayment, 2)}"; } }

        public decimal APR { get; set; }

        [DisplayName("Application Reference")]
        public string ApplicationRefrence { get; set; }
    }
}
