﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Loans.WebMvc.Models.Application
{
    public class LoanApplicationVM
    {
        [Required]
        public string Title { get; set; }

        [Required]
        [StringLength(20)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20)]
        public string Lastname { get; set; }

        public string FullName
        {
            get { return $"{FirstName} {Lastname}".Trim(); }
        }

        [Required]
        [StringLength(150)]
        public string Address { get; set; }

        [Required]
        [DisplayName("Mobile")]
        [RegularExpression(@"^(?:(?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)|(?:\(?0))(?:(?:\d{5}\)?[\s-]?\d{4,5})|(?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))|(?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})|(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}))(?:[\s-]?(?:x|ext\.?|\#)\d{3,4})?$",
            ErrorMessage = "Invalid phone number")]
        //[Phone]
        public string MobileNumber { get; set; }

        [Required]
        [DisplayName("Email")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
            ErrorMessage = "Invalid Email Address")]
        //[EmailAddress]
        public string EmailAddress { get; set; }

        [Required]
        [DisplayName("Annual Income (in thousands)")]
        [Range(1, 1000)]
        public int AnnualIncome { get; set; }

        [Required]
        [DisplayName("Home Owner")]
        public bool IsHomeOwner { get; set; }

        [DisplayName("Car Registration Number")]
        public string CarRegistrationNumber { get; set; }

        [Required]
        [DisplayName("Loan Request Amount")]
        [Range(1, 500000)]
        public int LoanRequestAmount { get; set; }

        public List<SelectListItem> Titles =>
           GetTitles();
        private List<SelectListItem> GetTitles()
        {
            var result = new List<SelectListItem>();
            result.Add(new SelectListItem { Text = "Mr", Value = "Mr" });
            result.Add(new SelectListItem { Text = "Mrs", Value = "Mrs" });
            result.Add(new SelectListItem { Text = "Miss", Value = "Miss" });
            result.Add(new SelectListItem { Text = "Ms", Value = "Ms" });
            result.Add(new SelectListItem { Text = "Dr", Value = "Dr" });
            result.Add(new SelectListItem { Text = "Other", Value = "Other" });

            return result;
        }
    }
}
