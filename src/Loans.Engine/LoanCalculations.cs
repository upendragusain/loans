﻿using System;

namespace Loans.Engine
{
    //https://www.handbook.fca.org.uk/handbook/CONC/App/1/2.html#DES157
    //https://www.cashfloat.co.uk/apr-explained/
    public class LoanCalculations : ILoanCalculations
    {
        public APRResult GetAPR(decimal monthlyInterestRate, int loanTerm, decimal loanAmount)
        {
            var monthly = monthlyInterestRate / 12 / 100;
            decimal start = 1;
            decimal length = 1 + monthly;
            int i = 0;
            for (i = 0; i < loanTerm; i++)
            {
                start *= length;
            }
            var payment = loanAmount * monthly / (1 - (1 / start));
            var totrepay = payment * i;

            return new APRResult(Math.Round(loanAmount, 2),
                Math.Round(totrepay, 2),
                Math.Round(totrepay / loanTerm, 2),
                18, Math.Round(monthlyInterestRate, 2));
        }
    }
}
