﻿namespace Loans.Engine
{
    public interface ILoanCalculations
    {
        APRResult GetAPR(decimal monthlyInterestRate, int loanTerm, decimal loanAmount);
    }
}