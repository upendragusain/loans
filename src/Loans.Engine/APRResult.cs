﻿namespace Loans.Engine
{
    public class APRResult
    {
        public APRResult(
            decimal creditAmount,
            decimal totalAmountPayable,
            decimal repaymentAmount,
            int repaymentTerm,
            decimal APR)
        {
            CreditAmount = creditAmount;
            TotalAmountPayable = totalAmountPayable;
            RepaymentAmount = repaymentAmount;
            RepaymentTerm = repaymentTerm;
            this.APR = APR;
        }

        public decimal CreditAmount { get; }
        public decimal TotalAmountPayable { get; }
        public decimal RepaymentAmount { get; }
        public int RepaymentTerm { get; }
        public decimal APR { get; }
    }
}